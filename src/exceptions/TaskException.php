<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/1/4
 * Time: 16:53
 */

namespace com_qqbsmall\exception;


class TaskException extends \Exception
{
    protected $workId;
    protected $taskId;

    public function __construct($message , $workerId , $taskId)
    {
        $this->workId = $workerId;
        $this->taskId = $taskId;
        $this->message = $message;
    }


    /**
     * @return mixed
     */
    public function getWorkId()
    {
        return $this->workId;
    }

    /**
     * @param mixed $workId
     */
    public function setWorkId($workId): void
    {
        $this->workId = $workId;
    }

    /**
     * @return mixed
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * @param mixed $taskId
     */
    public function setTaskId($taskId): void
    {
        $this->taskId = $taskId;
    }

    public function __toString()
    {
        return sprintf("[taskExceptions]workId[%d],taskId[%d] " . $this->message , $this->workId , $this->taskId);
    }
}