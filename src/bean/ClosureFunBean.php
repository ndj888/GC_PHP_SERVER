<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/26
 * Time: 19:36
 */

namespace com_qqbsmall\bean;
use com_jjcbs\lib\SimpleRpc;


/**
 * 注册server 相关 callback
 * Class ClosureFunBean
 * @package com_qqbsmall\rpc\bean
 */
class ClosureFunBean extends SimpleRpc
{
    /**
     * @var \Closure
     */
    protected $onRequestCallBack;
    /**
     * @var \Closure
     */
    protected $initCallBack;
    /**
     * task 执行任务方法
     * @var \Closure
     */
    protected $onTaskDoCallBack;
    /**
     * task 执行完回调
     * @var \Closure
     */
    protected $onTaskFinishCallBack;

    /**
     * @return \Closure
     */
    public function getOnRequestCallBack()
    {
        return $this->onRequestCallBack;
    }

    /**
     * @param \Closure $onRequestCallBack
     */
    public function setOnRequestCallBack($onRequestCallBack)
    {
        $this->onRequestCallBack = $onRequestCallBack;
    }

    /**
     * @return \Closure
     */
    public function getInitCallBack()
    {
        return $this->initCallBack;
    }

    /**
     * @param \Closure $initCallBack
     */
    public function setInitCallBack($initCallBack)
    {
        $this->initCallBack = $initCallBack;
    }

    /**
     * @return \Closure
     */
    public function getOnTaskDoCallBack(): \Closure
    {
        return $this->onTaskDoCallBack;
    }

    /**
     * @param \Closure $onTaskDoCallBack
     */
    public function setOnTaskDoCallBack(\Closure $onTaskDoCallBack): void
    {
        $this->onTaskDoCallBack = $onTaskDoCallBack;
    }

    /**
     * @return \Closure
     */
    public function getOnTaskFinishCallBack(): \Closure
    {
        return $this->onTaskFinishCallBack;
    }

    /**
     * @param \Closure $onTaskFinishCallBack
     */
    public function setOnTaskFinishCallBack(\Closure $onTaskFinishCallBack): void
    {
        $this->onTaskFinishCallBack = $onTaskFinishCallBack;
    }



}