<?php
/**
 * Created by PhpStorm.
 * User: longbob
 * Date: 2018/7/12 0012
 * Time: 14:35
 */

namespace com_qqbsmall\bean;
use com_jjcbs\lib\SimpleRpc;


/**
 * Class RpcServerConfig
 * @package com_jjcbs\rpc\bean
 */
class ServerConfigBean extends SimpleRpc
{
    /**
     * 最大服务注册size 默认2048满足一般需要了
     * @var int
     */
    protected $max_server_map_size = 2048;
    protected $listen = '0.0.0.0';
    protected $port = 80;
    /**
     * 是否是守护进程
     * @var int
     */
    protected $daemonize = 1;
    protected $reactor_num = 2;
    protected $worker_num = 4;
    protected $max_request = 0;
    /**
     * 包最大长度 default 10mb
     * @var int
     */
    protected $package_max_length = 10 * 1024 * 1024;
    /**
     * 最大处理连接数
     * @var int
     */
    protected $max_coon = 1000;
    /**
     * 轮询tcp时间
     * @var int
     */
    protected $heartbeat_check_interval = 30;
    /**
     * tcp 连接闲置时间 超过这个没有回应的连接会被回收
     * @var int
     */
    protected $heartbeat_idle_time = 60;
    /**
     * task 进程
     * @var int
     */
    protected $task_worker_num = 12;

    /**
     * @return int
     */
    public function getMaxServerMapSize()
    {
        return $this->max_server_map_size;
    }

    /**
     * @param int $max_server_map_size
     */
    public function setMaxServerMapSize($max_server_map_size)
    {
        $this->max_server_map_size = $max_server_map_size;
    }

    /**
     * @return string
     */
    public function getListen()
    {
        return $this->listen;
    }

    /**
     * @param string $listen
     */
    public function setListen($listen)
    {
        $this->listen = $listen;
    }

    /**
     * @return int
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param int $port
     */
    public function setPort($port)
    {
        $this->port = $port;
    }

    /**
     * @return int
     */
    public function getDaemonize()
    {
        return $this->daemonize;
    }

    /**
     * @param int $daemonize
     */
    public function setDaemonize($daemonize)
    {
        $this->daemonize = $daemonize;
    }


    /**
     * @return int
     */
    public function getReactorNum()
    {
        return $this->reactor_num;
    }

    /**
     * @param int $reactor_num
     */
    public function setReactorNum($reactor_num)
    {
        $this->reactor_num = $reactor_num;
    }

    /**
     * @return int
     */
    public function getWorkerNum()
    {
        return $this->worker_num;
    }

    /**
     * @param int $worker_num
     */
    public function setWorkerNum($worker_num)
    {
        $this->worker_num = $worker_num;
    }

    /**
     * @return int
     */
    public function getMaxRequest()
    {
        return $this->max_request;
    }

    /**
     * @param int $max_request
     */
    public function setMaxRequest($max_request)
    {
        $this->max_request = $max_request;
    }

    /**
     * @return int
     */
    public function getMaxCoon()
    {
        return $this->max_coon;
    }

    /**
     * @param int $max_coon
     */
    public function setMaxCoon($max_coon)
    {
        $this->max_coon = $max_coon;
    }

    /**
     * @return int
     */
    public function getHeartbeatCheckInterval()
    {
        return $this->heartbeat_check_interval;
    }

    /**
     * @param int $heartbeat_check_interval
     */
    public function setHeartbeatCheckInterval($heartbeat_check_interval)
    {
        $this->heartbeat_check_interval = $heartbeat_check_interval;
    }

    /**
     * @return int
     */
    public function getHeartbeatIdleTime()
    {
        return $this->heartbeat_idle_time;
    }

    /**
     * @param int $heartbeat_idle_time
     */
    public function setHeartbeatIdleTime($heartbeat_idle_time)
    {
        $this->heartbeat_idle_time = $heartbeat_idle_time;
    }

    /**
     * @return int
     */
    public function getPackageMaxLength()
    {
        return $this->package_max_length;
    }

    /**
     * @param int $package_max_length
     */
    public function setPackageMaxLength($package_max_length)
    {
        $this->package_max_length = $package_max_length;
    }

    /**
     * @return int
     */
    public function getTaskWorkerNum(): int
    {
        return $this->task_worker_num;
    }

    /**
     * @param int $task_worker_num
     */
    public function setTaskWorkerNum(int $task_worker_num): void
    {
        $this->task_worker_num = $task_worker_num;
    }


}