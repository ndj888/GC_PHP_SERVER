<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/1/4
 * Time: 16:48
 */

namespace com_qqbsmall\bean;


use com_jjcbs\lib\SimpleRpc;

/**
 * Task消息
 * Class TaskMessageBean
 * @package com_qqbsmall\bean
 */
class TaskMessageBean extends SimpleRpc
{
    protected $className;
    protected $message;
    /**
     * @return mixed
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * @param mixed $className
     */
    public function setClassName($className): void
    {
        $this->className = $className;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message): void
    {
        $this->message = $message;
    }





}