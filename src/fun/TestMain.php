<?php
/**
 * Created by jjc.
 * User: longbob
 * Date: 2018/9/28
 * Time: 12:50
 */

namespace com_qqbsmall\fun;

class TestMain
{
    public static function writeLog($data = ''){
        $fptr = fopen(BASE_ROOT_PATH . '/test.log' , 'a+');
        fwrite($fptr , '['.date('Y-m-d H:i:s').']' . $data . "\n\n");
        fclose($fptr);
    }
}