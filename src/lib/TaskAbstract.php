<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/1/4
 * Time: 17:04
 */

namespace com_qqbsmall\lib;


/**
 * TaskAbstract
 * Class TaskAbstract
 * @package com_qqbsmall\lib
 */
abstract class TaskAbstract
{
    protected $taskId;
    protected $workId;
    /**
     * 消息参数
     * @var mixed
     */
    protected $message;


    public function __construct(int $taskId , int $workId , $message)
    {
        $this->taskId = $taskId;
        $this->workId = $workId;
        $this->message = $message;
    }




    /**
     * 执行task异步任务
     * @return mixed
     */
    abstract public function exec();

    public function callBack(){

    }
}