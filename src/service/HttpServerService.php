<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/26
 * Time: 19:18
 */

namespace com_qqbsmall\service;

use com_jjcbs\lib\Service;
use com_jjcbs\lib\ServiceFactory;
use com_qqbsmall\bean\TaskMessageBean;
use com_qqbsmall\exception\TaskException;
use com_qqbsmall\lib\TaskAbstract;
use Swoole\Http\Request;


/**
 * Http Server
 * Class HttpServerService
 * @package com_qqbsmall\service
 */
class HttpServerService extends Service
{
    /**
     * @var \swoole_http_server
     */
    private $serv;
    /**
     * @var ClosureFunBean
     */
    public static $closureFunBean;

    public function __construct($serverConfigBean, $closureFunBean)
    {
        self::$closureFunBean = $closureFunBean;
        $this->serv = new \swoole_http_server(
            $serverConfigBean->getListen(),
            $serverConfigBean->getPort()
        );
        // set config env
        $this->serv->set($serverConfigBean->toArray());
    }

    /**
     * 初始化 swoole_table task 等投递任务
     * @param \Closure $fun
     */
    protected function init($fun)
    {
        // 设置跨域访问
//        ServiceFactory::getInstance(ResponseService::class)->setHeader([
//            'key' => 'Access-Control-Allow-Origin',
//            'val' => '*'
//        ]);
        $fun();
    }

    public function start()
    {
        $this->serv->on('request', function ($request, $response) {
            try {
                //注册task管理
                $taskManagerService = ServiceFactory::getInstance(TaskManagerService::class);
                $taskManagerService->setSer($this->serv);
                //task set end
                $responseService = ServiceFactory::getInstance(ResponseService::class);
                // not php request pass
                if (strpos($request->server['request_uri'], 'favicon') !== false) {
                    return $response->end(json_encode([]));
                }
//                TestMain::writeLog(json_encode($request->server));
                $fun = self::$closureFunBean->getOnRequestCallBack();
                $this->setRequestContent($request);
                /**
                 * $data ['header' => [] , 'body' => '' , 'cookie' => []]
                 */
                $fun();
                $headers = $responseService->getHeader();
                $cookies = $responseService->getCookie();
                foreach ($headers as $k => $v) {
                    $response->header($k, $v);
                }
                foreach ($cookies as $v) {
                    $response->cookie($v['name'], $v['value'], $v['expire'], $v['path'], $v['domain'], $v['secure']);
                }
//                TestMain::writeLog('输出的响应是:' . $responseService->getBody());
                $response->end($responseService->getBody());
                // clean response、request
                $this->cleanRequestContent();
                $responseService->clean();
                return;
            } catch (\Exception $e) {
                $response->header('Content-Type', 'application/json');
                $response->end(json_encode([
                    'msg' => 'Error[' . $e . ']'
                ]));
                return;
            }
        });
        $this->serv->on("WorkerStart", function ($server, $worker_id) {
            global $serverConfig;
            try{
                if ($worker_id < $serverConfig['worker_num']) {
                    //event worker
                    $this->init(self::$closureFunBean->getInitCallBack());
                }else{
                    // task worker
                    $this->init(self::$closureFunBean->getInitCallBack());
                }
            }catch (\Throwable $e){
                echo "启动期异常[" . $e->getMessage() . "]";
                exit;
            }
        });

        $this->serv->on('Task', function (\swoole_server $server, $task_id, $from_id, $data) {
            $taskMessage = unserialize($data);
            $className = $taskMessage->getClassName();
            $obj = new $className($task_id , $from_id , $taskMessage->getMessage());
            if (!($obj instanceof TaskAbstract)) {
                throw new TaskException("非task派生类，不能执行task操作", $task_id, $from_id);
            }
            try{
                $obj->exec();
            }catch (\Exception $e){
                throw new TaskException($task_id , $from_id , $e->getMessage());
            }
            $server->finish(serialize($obj));
        });

        $this->serv->on('Finish', function (\swoole_server $serv, $task_id, $data) {
            $taskObj = unserialize($data);
            if ( !($taskObj instanceof  TaskAbstract)){
                throw new \Exception('[worker]task must be implement TaskAbstract');
            }
            try{
                $taskObj->callBack();
            }catch (\Exception $e){
                throw new \Exception('[worker]task call back exception[' . $e->getMessage() . ']taskId:' . $task_id);
            }
        });
        $this->serv->start();
    }

    /**
     * 设置request 上下文给 $_POST , $_GET ,$_REQUEST , $_SERVER
     * @param $request
     */
    public function setRequestContent(Request $request)
    {

        $_SERVER = $request->server;
        $_POST = isset($request->post) ? $request->post : [];
        $_GET = isset($request->get) ? $request->get : [];
        $_COOKIE = isset($request->cookie) ? $request->cookie : [];
        $_FILES = isset($request->files) ? $request->files : [];
        $_REQUEST = array_merge($_GET, $_POST, [
            'raw' => $request->rawContent()
        ]);
    }

    /**
     * clean request 上下文
     */
    public function cleanRequestContent()
    {
        $_SERVER = null;
        $_POST = null;
        $_GET = null;
        $_COOKIE = null;
        $_FILES = null;
        $_REQUEST = null;
    }


    public function exec()
    {
        // TODO: Implement exec() method.
    }

}
