<?php
/**
 * Created by jjc.
 * User: longbob
 * Date: 2018/9/27
 * Time: 18:28
 */

namespace com_qqbsmall\service;
use com_jjcbs\lib\Service;


/**
 * 单例模式，改造响应
 * Class ResponseService
 * @package com_qqbsmall\service
 */
class ResponseService extends Service
{
    protected  $header = [];
    protected  $cookie = [];
    protected  $body = '';

    /**
     * @return array
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * @param array $header
     */
    public function setHeader($header)
    {
        $this->header[$header['key']] = $header['val'];
    }

    /**
     * @return array
     */
    public function getCookie()
    {
        return $this->cookie;
    }

    /**
     * @param $name
     * @param $value
     * @param string $expire
     * @param string $path
     * @param string $domain
     * @param bool $secure
     */
    public function setCookie($name, $value, $expire = '3600', $path = '', $domain = '', $secure = false)
    {
        array_push($this->cookie, [
            'name' => $name,
            'value' => $value,
            'expire' => $expire,
            'path' => $path,
            'domain' => $domain,
            'secure' => $secure
        ]);
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }


    public function clean()
    {
        $this->body = '';
        $this->cookie = [];
        $this->header = [];
    }

    public function exec()
    {
        // TODO: Implement exec() method.
    }

}
