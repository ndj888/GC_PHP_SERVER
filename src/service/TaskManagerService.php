<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/1/4
 * Time: 17:24
 */

namespace com_qqbsmall\service;


use com_jjcbs\lib\Service;
use com_qqbsmall\bean\TaskMessageBean;
use com_qqbsmall\lib\TaskAbstract;

/**
 * Task任务管理
 * Class TaskManagerService
 * @package com_qqbsmall\service
 */
class TaskManagerService extends Service
{
    /**
     * @var \swoole_server
     */
    protected $ser;

    /**
     * @param \swoole_server $ser
     */
    public function setSer(\swoole_server $ser): void
    {
        $this->ser = $ser;
    }

    /**
     * @return \swoole_server
     */
    public function getSer(): \swoole_server
    {
        return $this->ser;
    }


    /**
     * @param TaskMessageBean $taskMessageBean
     * @throws \Exception
     * @return bool
     */
    public function pushTask(TaskMessageBean $taskMessageBean) : bool {
        return $this->ser->task(serialize($taskMessageBean)) === false ? false : true;
    }
    public function exec()
    {
        // TODO: Implement exec() method.
    }

}