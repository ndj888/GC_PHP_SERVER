## 传统项目自动适配swoole项目
### 适用项目
1. 使用传统php解决方案构建的项目，不能使用框架本身升级来完成支持swoole模式开发
2. 项目性能存在一定瓶颈的老项目，需要更高的io和并发支持

### 使用说明
````
//新建立server.php文件，为项目启动入口文件
/**
 * SWOOLE server api 服务
 */
define('APP_ID','api');
define('BASE_PATH',str_replace('\\','/',dirname(__FILE__)));
if (!@include(dirname(dirname(__FILE__)).'/global.php')) exit('global.php isn\'t exists!');
if (!@include(BASE_CORE_PATH.'/33hao.php')) exit('33hao.php isn\'t exists!');
if (!@include(BASE_PATH.'/config/config.ini.php')){
    @header("Location: install/index.php");die;
}

/* define('APP_SITE_URL',CIRCLE_SITE_URL);
define('TPL_NAME', TPL_CIRCLE_NAME);
define('CIRCLE_TEMPLATES_URL', CIRCLE_SITE_URL.'/templates/'.TPL_NAME);
define('CIRCLE_RESOURCE_SITE_URL',CIRCLE_SITE_URL.'/resource'); */

require(BASE_PATH.'/framework/function/function.php');
require(BASE_PATH.'/control/base.php');
define('ROOT_PATH' , dirname(dirname(__FILE__)));
// add user Controller
//require BASE_PATH . '/control/user.php';
require ROOT_PATH . '/vendor/autoload.php';
$serverConfig = require ROOT_PATH . '/serverConfig.php';
if ( empty($serverConfig)){
    echo 'server config error';
    die;
}
$requestService = \com_jjcbs\lib\ServiceFactory::getInstance(\com_qqbsmall\service\ResponseService::class);
// server config
$serverConfigBean = new \com_qqbsmall\bean\ServerConfigBean();
$serverConfigBean->setListen($serverConfig['listen']);
$serverConfigBean->setPort($serverConfig['port']);
$serverConfigBean->setWorkerNum($serverConfig['worker_num']);
$serverConfigBean->setReactorNum($serverConfig['reactor_num']);
$serverConfigBean->setMaxRequest($serverConfig['max_request']);
$serverConfigBean->setMaxCoon($serverConfig['max_coon']);
$serverConfigBean->setDaemonize($serverConfig['daemonize']);


// function

$closeFunBean = new \com_qqbsmall\bean\ClosureFunBean();
$closeFunBean->setInitCallBack(function (){
    //init framework
    Base::init();
    // 预先生成ORM cache 信息
//    Model::fetchTablePkArray();
});

$closeFunBean->setOnRequestCallBack(function (){
    try{
        Base::control();
    }catch (responseandexception $e){
        output_json($e->getData());
    }
    catch (Exception $e){
        output_json($e->getMessage());
    }
});

// response service
$server = \com_jjcbs\lib\ServiceFactory::getInstance(\com_qqbsmall\service\HttpServerService::class , [
    $serverConfigBean,
    $closeFunBean
]);


$server->start();
````
### 注意事项
1. 用于swoole 的特性以下函数将禁止使用 die exit echo sleep